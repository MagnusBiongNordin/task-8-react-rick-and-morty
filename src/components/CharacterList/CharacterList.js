import React from "react";
import styles from "./CharacterList.module.css";
import Character from "../Character/Character"

const API_URL = "https://rickandmortyapi.com/api/character/";

class CharacterList extends React.Component {
  state = {
    characters: [],
    lastSearchWord: "",
    nextFetch: ""
  };

  async componentDidMount() {
    try {
      const response = await fetch(API_URL).then(resp => resp.json());
      let chars = response.results;

      this.setState({
        characters: chars,
        nextFetch: response.info.next
      });
    } catch (e) {
      console.error(e);
    }
  }

  async componentDidUpdate() {
    try {
      document.addEventListener('scroll', this.trackScrolling);

      if(this.state.lastSearchWord !== this.props.searchWord) {
        const response = await fetch(API_URL+"?name=" +this.props.searchWord).then(resp => resp.json());
        
        if(response.results !== undefined) {
          let chars = response.results;
          this.setState({
            characters: chars,
            lastSearchWord: this.props.searchWord,
            nextFetch: response.info.next
          })
        } else {
          this.setState({
            characters: [],
            lastSearchWord: this.props.searchWord,
            nextFetch: response.info.next
          })
        }
      }
    } catch (error) {
      console.error(error);
    }
  }
  
  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
  }

  async getMoreCharacters() {
    try {
      const response = await fetch(this.state.nextFetch).then(resp => resp.json());
      let char = this.state.characters;
      char = char.push(...response.results)
      this.setState({
        character: char,
        nextFetch: response.info.next
      })
    } catch (error) {
      console.error(error);
    }
  }

  isBottom(el) {
    return el.getBoundingClientRect().bottom <= window.innerHeight;
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById('characterListId');
    if (this.isBottom(wrappedElement)) {
      document.removeEventListener('scroll', this.trackScrolling);
      this.getMoreCharacters();
    }
  };


  
  render() {
    const characters = this.state.characters.map(character => {
        return (
          <Character character={character} key={character.id}/>
        );
    });

    return (
      <div className={styles.CharacterList} id="characterListId">
        <div className={styles.Characters}>{characters}</div>
      </div>
    );
  }
}

export default CharacterList;