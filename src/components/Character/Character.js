import React from 'react';
import { Link } from "react-router-dom";
import styles from "./Character.module.css";


const Character = props =>  {
    return (
        <div className={styles.card}>
            <img src={props.character.image} alt={props.character.name}/>
            <div className={styles.characterinfo}>
                <div className={styles.container}>
                    <h4><b>{props.character.name}</b></h4>
                    <p>Gender: {props.character.gender}</p>
                    <p>Status: {props.character.status}</p>
                    <p>Species: {props.character.species}</p>
                </div>
                <Link className={styles.linkToCharacter} style={{ textDecoration: 'none' }} to={{
                        pathname: "/profile",
                        state: {
                            character: props.character
                        }}}
                    >+</Link>
               

            </div>
        </div>
    )
};

export default Character;