import React from "react";
import styles from "./SearchBar.module.css";

class SearchBar extends React.Component {
    getSearchValue = evt => {
        this.props.handleGetSearchBarWord(evt.target.value);
    }

    render() {
        return(
            <input className={styles.InputSearch} onChange={this.getSearchValue} placeholder="Search.."></input>
        )
    }
}

export default SearchBar;