import React from "react";
import CharacterList from '../components/CharacterList/CharacterList';
import SearchBar from '../components/SearchBar/SearchBar';
import styles from './MainView.module.css';
import titleImg from '../title.png';

class MainView extends React.Component {

    state = {
        searchWord: ""
    };

    constructor(props) {
        super(props);
        this.getSearchBarWord = this.getSearchBarWord.bind(this);
    }
    
    getSearchBarWord(value) {
        try {    
          this.setState({
            searchWord: value
          });
        } catch (e) {
          console.error(e);
        }
    }
    
    render() {
        return(
            <div className={styles.MainView}>
                <img src={titleImg} className={styles.Titleimg}/>
                <SearchBar handleGetSearchBarWord={this.getSearchBarWord}/>
                <CharacterList searchWord={this.state.searchWord}/>
            </div>
        )
    }
}

export default MainView;