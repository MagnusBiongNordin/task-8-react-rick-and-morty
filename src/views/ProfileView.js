import React from "react";
import styles from './ProfileView.module.css';

const ProfileView = props => {
    const character = props.location.state.character;

    let i = 0;
    var episodeNumbers = character.episode.map(episode => {
        let epNumber = episode.split('episode/');
        i++;
        if(i === character.episode.length) {
            return epNumber[1] + '.';
        } else {
            return epNumber[1] + ', ';
        }
    })
    
    var type = character.type ? <p><b>Type: </b>{character.type}</p> : <p> </p>;

    return (
        <div className={styles.Profile}>
            <div className={styles.CharacterInfo}>
                <h1><b>{character.name}</b></h1>
                <br></br>
                <p><b>Gender: </b>{character.gender}</p>
                <p><b>Status: </b>{character.status}</p>
                <p><b>Species: </b>{character.species}</p>
                {type}
                <p><b>Origin: </b>{character.origin.name}</p>
                <p><b>Location: </b>{character.location.name}</p>
                <p className={styles.EpisodeNumber}><b>In episodes: </b>{episodeNumbers}</p>
            </div>
            <div className={styles.ImageBox}>
                <img src={character.image} alt={character.name} className={styles.Image}/>
            </div>
        </div>
    )
}

export default ProfileView;

