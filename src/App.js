import React from 'react';
import './App.css';
import ProfileView from './views/ProfileView';
import MainView from './views/MainView';
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";


function App() {

  return (
    <Router>
      <div className="App">
        <main>
          <Switch>
            <Route exact path="/" component={MainView}/>
            <Route path="/profile" component={ProfileView}/>
          </Switch>
        </main>
      </div>
    </Router>

  );
}

export default App;