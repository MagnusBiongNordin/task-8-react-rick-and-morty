﻿## Screens

![Main View](https://gitlab.com/MagnusBiongNordin/task-8-react-rick-and-morty/raw/master/Screenshots/Screen%20MainVeiw.png "Main View")

![Main View with search](https://gitlab.com/MagnusBiongNordin/task-8-react-rick-and-morty/raw/master/Screenshots/Screen%20MainView%20Search.png "Main View with Search")

![Character Profile](https://gitlab.com/MagnusBiongNordin/task-8-react-rick-and-morty/raw/master/Screenshots/Screen%20ProfileVIew.png "Character Profile")

![Character profile with hover over image](https://gitlab.com/MagnusBiongNordin/task-8-react-rick-and-morty/raw/master/Screenshots/Screen%20ProfileView%20Hover%20Img.png "Character Profile with hover on img")

## Views

### MainView
Renders the SearchBar and CharacterList components.
Stores a search word is its state, updated from the SearchBar component.

### ProfileView
Renders the character profile page. It is initated from a route-link in the Character component and provided the current character to be rendered.

## Components

### Character
Renders each character as a "card". Each card contains basic profile information and a button that routes to the profile view.
Cool stuff: Hover image to get color back. Hover episode list to view all episodes. 

### CharacterList 
Uses the Character component to render a list of Characters, which are fetched from the rick and morty API.
If a search word is provided from the SearchBar component, the characterList will render a subset of the characters, filtered on the search word.
Updated: Will fetch new characters when scrolled to buttom.

### Searchbar
Renders a search bar, which onChange sends the target value up to the parent component

## Challanges, findings and difficulties encountered in the project

### Fetching
Originally, I fetched all available characters in one fetch in the CharacterList Component and filtered on them with a given searchword. This was possible since the searchBar was initially rendered within the CharacterList component, giving the component direct access to the searchword. However, this didnt follow the structure provided by the tasks, changes needed to be done. New changes: the mainview rendered the searchbar and the characterlist in the same view. When rendering the CharacterList with the searchword as a prop became a challange. Updating the searchWord would re-render the CharacterList, making it re-fetch the character list each time.
This could have been fixed by fetching the characters and storing them in the MainView. However, I solved this issue by fetching a new list of characters based on the provided searchWord, instead of filtering in the already existing list.
And to view all characters in the mainview, I added a fetch function that runs when userscrolls to the end of the page.

### Component structure
The first working version of the application was built without the MainView component. The CharacterList contained both the SearchBar and the Character components. As this didnt concur with the task's given structure, I had to refactor the code. As described, this issue created more issues with the fetching and filtering of characters, and required a lot more work than anticipated. 

Lesson learned: read the task before starting coding, and plan the structure, the app's components and views, and how they are related. 